;;; svg-clock-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (svg-clock) "svg-clock" "svg-clock.el" (21295 29615
;;;;;;  0 0))
;;; Generated autoloads from svg-clock.el

(autoload 'svg-clock "svg-clock" "\
Start/stop the svg clock.

\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("svg-clock-pkg.el") (21295 29615 806225
;;;;;;  0))

;;;***

(provide 'svg-clock-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; svg-clock-autoloads.el ends here
