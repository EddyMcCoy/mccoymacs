;;; skewer-reload-stylesheets-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (skewer-reload-stylesheets-mode) "skewer-reload-stylesheets"
;;;;;;  "skewer-reload-stylesheets.el" (21305 18294 0 0))
;;; Generated autoloads from skewer-reload-stylesheets.el

(autoload 'skewer-reload-stylesheets-mode "skewer-reload-stylesheets" "\
Minor mode for interactively reloading CSS stylesheets.

\(fn &optional ARG)" t nil)

;;;***

;;;### (autoloads nil nil ("skewer-reload-stylesheets-pkg.el") (21305
;;;;;;  18294 417155 0))

;;;***

(provide 'skewer-reload-stylesheets-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; skewer-reload-stylesheets-autoloads.el ends here
