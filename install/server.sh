#!/bin/sh
# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    server.sh                                          :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: gfanton <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/05/27 15:25:21 by gfanton           #+#    #+#              #
#    Updated: 2014/05/27 15:25:31 by gfanton          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

if [[ -n $(ps aux | grep "[E]macs.*--daemon") ]]
then
    echo "Emacs is running"
else
    echo "Starting Emacs Daemon"
    ~/Applications/Emacs.app/Contents/MacOS/Emacs --daemon
    clear
    echo "Emacs is now running"
fi
