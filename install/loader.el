;;; loader.el --- first load config
;;; commentary:
;******************************************************************************;
;                                                                              ;
;                                                         :::      ::::::::    ;
;    loader.el                                          :+:      :+:    :+:    ;
;                                                     +:+ +:+         +:+      ;
;    By: gfanton <gfanton@student.42.fr>            +#+  +:+       +#+         ;
;                                                 +#+#+#+#+#+   +#+            ;
;    Created: 2014/04/14 18:49:18 by gfanton           #+#    #+#              ;
;    Updated: 2014/04/14 19:22:46 by gfanton          ###   ########.fr        ;
;                                                                              ;
;******************************************************************************;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;						;;
;;	Emacs config for Emacs 24.3.1		;;
;;						;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; code:
;;;;;;;;;;;;;;;;;;;;;; load general features files
(setq config_files "/usr/share/emacs/site-lisp/")
(setq load-path (append (list nil config_files) load-path))
(add-to-list 'load-path "~/.emacs.d/")
(when (>= emacs-major-version 24)
  (require 'package)
  (package-initialize)
  (add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/") t)
  )
;;;;;;;;;;;;;;;;;;;;;; Term mode
(require 'myterm)
(load "config.el")
;;;;;;;;;;;;;;;;;;;;;; other




(provide 'loader)
;;; loader.el ends here
