#!/bin/sh ******************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    install.sh                                         :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: gfanton <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/05/27 13:49:48 by gfanton           #+#    #+#              #
#    Updated: 2014/05/27 16:00:40 by gfanton          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

MOUNTPOINT="/Volumes/emacs"
FTMP=~/tmp$RANDOM
DNAME=emacs
IFS="
"

if [ ! -n "$EMA" ]; then
  EMA=~/.emacs.d
fi

if [ -d "$EMA" ]; then
    echo "You already have a \033[1;36m.emacs.d\033[0m"
    echo "if you want to continue please delete your \033[1;36m.emacs.d\033[0m"
	i="None"
	while [ $i != "y" ] && [ $i != "n" ]; do
		echo "delete now ? [y/n]"
		read i
	done
	if [ $i = 'y' ]; then
		rm -rf ~/.emacs.d
	fi
fi

echo ""
echo "\033[1;36m                                     dddddddd           dddddddd                     \033[0m"
echo "\033[1;36m    EEEEEEEEEEEEEEEEEEEEEE           d::::::d           d::::::d                     \033[0m"
echo "\033[1;36m    E::::::::::::::::::::E           d::::::d           d::::::d                     \033[0m"
echo "\033[1;36m    E::::::::::::::::::::E           d::::::d           d::::::d                     \033[0m"
echo "\033[1;36m    EE::::::EEEEEEEEE::::E           d:::::d            d:::::d                      \033[0m"
echo "\033[1;36m      E:::::E       EEEEEE   ddddddddd:::::d    ddddddddd:::::yyyyyyy           yyyyyyy\033[0m"
echo "\033[1;36m      E:::::E              dd::::::::::::::d  dd::::::::::::::dy:::::y         y:::::y \033[0m"
echo "\033[1;36m      E::::::EEEEEEEEEE   d::::::::::::::::d d::::::::::::::::d y:::::y       y:::::y\033[0m"
echo "\033[1;36m      E:::::::::::::::E  d:::::::ddddd:::::dd:::::::ddddd:::::d  y:::::y     y:::::y \033[0m"
echo "\033[1;36m      E:::::::::::::::E  d::::::d    d:::::dd::::::d    d:::::d   y:::::y   y:::::y  \033[0m"
echo "\033[1;36m      E::::::EEEEEEEEEE  d:::::d     d:::::dd:::::d     d:::::d    y:::::y y:::::y   \033[0m"
echo "\033[1;36m      E:::::E            d:::::d     d:::::dd:::::d     d:::::d     y:::::y:::::y    \033[0m"
echo "\033[1;36m      E:::::E       EEEEEd:::::d     d:::::dd:::::d     d:::::d      y:::::::::y     \033[0m"
echo "\033[1;36m    EE::::::EEEEEEEE:::::d::::::ddddd::::::dd::::::ddddd::::::dd      y:::::::y      \033[0m"
echo "\033[1;36m    E::::::::::::::::::::Ed:::::::::::::::::dd:::::::::::::::::d       y:::::y       \033[0m"
echo "\033[1;36m    E::::::::::::::::::::E d:::::::::ddd::::d d:::::::::ddd::::d      y:::::y        \033[0m"
echo "\033[1;36m    EEEEEEEEEEEEEEEEEEEEEE  ddddddddd   ddddd  ddddddddd   dddd      y:::::y         \033[0m"
echo "\033[1;36m                         MMMMMMMM               MMMMMMMM            y:::::y          \033[0m"
echo "\033[1;36m                         M:::::::M             M:::::::M           y:::::y           \033[0m"
echo "\033[1;36m                         M::::::::M           M::::::::M          y:::::y            \033[0m"
echo "\033[1;36m                         M:::::::::M         M:::::::::M         y:::::y             \033[0m"
echo "\033[1;36m                         M::::::::::M       M::::::::::M   cccccccccccccccc          \033[0m"
echo "\033[1;36m                         M:::::::::::M     M:::::::::::M cc:::::::::::::::c          \033[0m"
echo "\033[1;36m                         M:::::::M::::M   M::::M:::::::Mc:::::::::::::::::c          \033[0m"
echo "\033[1;36m                         M::::::M M::::M M::::M M::::::c:::::::cccccc:::::c          \033[0m"
echo "\033[1;36m                         M::::::M  M::::M::::M  M::::::c::::::c     ccccccc          \033[0m"
echo "\033[1;36m                         M::::::M   M:::::::M   M::::::c:::::c                       \033[0m"
echo "\033[1;36m                         M::::::M    M:::::M    M::::::c:::::c                       \033[0m"
echo "\033[1;36m        CCCCCCCCCCCCC    M::::::M     MMMMM     M::::::c::::::c     ccccccc          \033[0m"
echo "\033[1;36m     CCC::::::::::::C    M::::::M               M::::::c:::::::cccccc:::::c          \033[0m"
echo "\033[1;36m   CC:::::::::::::::C    M::::::M               M::::::Mc:::::::::::::::::c          \033[0m"
echo "\033[1;36m  C:::::CCCCCCCC::::C    M::::::M               M::::::M cc:::::::::::::::c          \033[0m"
echo "\033[1;36m C:::::C       CCCCCC  oooooooooooyyyyyyy       MMMMyyyyyyycccccccccccccccc          \033[0m"
echo "\033[1;36mC:::::C              oo:::::::::::oy:::::y         y:::::y                           \033[0m"
echo "\033[1;36mC:::::C             o:::::::::::::::y:::::y       y:::::y                            \033[0m"
echo "\033[1;36mC:::::C             o:::::ooooo:::::oy:::::y     y:::::y                             \033[0m"
echo "\033[1;36mC:::::C             o::::o     o::::o y:::::y   y:::::y                              \033[0m"
echo "\033[1;36mC:::::C             o::::o     o::::o  y:::::y y:::::y                               \033[0m"
echo "\033[1;36mC:::::C             o::::o     o::::o   y:::::y:::::y                                \033[0m"
echo "\033[1;36m C:::::C       CCCCCo::::o     o::::o    y:::::::::y                                 \033[0m"
echo "\033[1;36m  C:::::CCCCCCCC::::o:::::ooooo:::::o     y:::::::y                                  \033[0m"
echo "\033[1;36m   CC:::::::::::::::o:::::::::::::::o      y:::::y                                   \033[0m"
echo "\033[1;36m     CCC::::::::::::Coo:::::::::::oo      y:::::y                                    \033[0m"
echo "\033[1;36m        CCCCCCCCCCCCC  ooooooooooo       y:::::y                                     \033[0m"
echo "\033[1;36m                                        y:::::y                                      \033[0m"
echo "\033[1;36m                                       y:::::y                                       \033[0m"
echo "\033[1;36m                                      y:::::y                                        \033[0m"
echo "\033[1;36m                                     y:::::y                                         \033[0m"
echo "\033[1;36m                                    yyyyyyy                                          \033[0m"

echo ""
echo ""
echo "\033[1;32m-->cloning McCoyMacs... \033[0m"
hash git >/dev/null && /usr/bin/env git clone https://bitbucket.org/EddyMcCoy/mccoymacs.git $EMA || {
  echo "git not installed"
  exit 1
}

echo ""
echo "\033[1;32m-->creating tmp folder... \033[0m"

if [ ! -d $FTMP ]; then
    if mkdir $FTMP; then
		echo "ok !"
	else
		echo "fail !"
		exit 1
	fi
else
	echo "unluky $FTMP already exit, restart this script"
	exit 1
fi

DMG=$FTMP/$DNAME.dmg

echo ""
echo "\033[1;32m-->downloading emacs 24.3 in $DMG...\033[0m"

if curl -L -o $DMG --progress-bar -O "http://emacsformacosx.com/emacs-builds/Emacs-24.3-universal-10.6.8.dmg"; then
	echo "ok !"
else
	echo "fail !"
	exit 1
fi

echo ""
echo "\033[1;32m-->mounting $DMG...\033[0m"
hdiutil attach -nobrowse -noautoopenro -mountpoint $MOUNTPOINT $DMG
app=$MOUNTPOINT/Emacs.app
if [ ! -d "~/Applications" ]; then
    mkdir ~/Applications
fi

echo "\033[1;32m-->installing emacs...\033[0m"

cp -v -r $app ~/Applications

echo ""
echo "\033[1;32m-->unmounting $DMG...\033[0m"

hdiutil detach $MOUNTPOINT

echo ""
echo "\033[1;32m-->setting up env...\033[0m"

echo "--importing loader"
cp ~/.emacs.d/install/loader.el ~/.emacs.d/loader.mccoy
if [ -f ~/.emacs ]; then
	echo ".emacs already exist"
	mv ~/.emacs ~/.emacs.old
	echo "saving .emacs -> .emacs.old"
fi
printf "linking .emacs:\033[0m"
ln -Fs ~/.emacs.d/loader.mccoy ~/.emacs
echo "ok !"

printf "creating emacs alias (Emacs -nw) or e... "
echo "alias emacs='~/Applications/Emacs.app/Contents/MacOS/bin/emacsclient -nw'" >> ~/.zshrc
echo "alias e='~/Applications/Emacs.app/Contents/MacOS/bin/emacsclient -nw'" >> ~/.zshrc
echo "ok !"

printf "creating qmacs alias (Emacs -q -nw) or q... "
echo "alias qmacs='~/Applications/Emacs.app/Contents/MacOS/Emacs -q -nw'" >> ~/.zshrc
echo "alias q='~/Applications/Emacs.app/Contents/MacOS/Emacs -q -nw'" >> ~/.zshrc
echo "ok !"

printf "seting xterm-256color... "
echo 'TERM=xterm-256color' | cat - ~/.zshrc > $FTMP/term && mv $FTMP/term ~/.zshrc
echo "ok !"

printf "adding mail: $USER@student.42.fr... "
echo "export MAIL='$USER@student.42.fr'" >> ~/.zshrc
echo "ok !"

printf "setup Daemon Emacs... "
ln -Fs ~/.emacs.d/install/server.sh ~/.emacs_daemon
chmod 755 ~/.emacs_daemon
echo "~/.emacs.d/install/server.sh" >> ~/.zshrc
echo "ok !"

printf "removing tmp... "
rm -rf $FTMP
echo "ok !"

echo ""
echo ""
echo ""
echo "\033[1;36m restart your term !\033[0m"
echo ""
echo ""
echo "\033[1;31m ============================\033[0m"
echo "\033[1;31m|           [DONE]           |\033[0m"
echo "\033[1;31m ============================\033[0m"
