;;; config.el --- config
;;; commentary:
;******************************************************************************;
;                                                                              ;
;                                                         :::      ::::::::    ;
;    config.el                                          :+:      :+:    :+:    ;
;                                                     +:+ +:+         +:+      ;
;    By: gfanton <gfanton@student.42.fr>            +#+  +:+       +#+         ;
;                                                 +#+#+#+#+#+   +#+            ;
;    Created: 2014/04/14 19:20:57 by gfanton           #+#    #+#              ;
;    Updated: 2014/05/27 12:15:16 by gfanton          ###   ########.fr        ;
;                                                                              ;
;******************************************************************************;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;						;;
;;	Emacs config for Emacs 24.3.1		;;
;;						;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; code:
;;;;;;;;;;;;;;;;;;;;;; load basic
(add-to-list 'load-path "~/.emacs.d/lisp/")
(load "string.el")
(load "comments.el")
(load "list.el")
(load "del-end-whitespace.el")
(load "header.el")
(load "user-scrollup.el")
(set-language-environment "UTF-8")
(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
(setq fill-column 80)
(setq text-mode-hook 'turn-on-auto-fill)
(setq major-mode 'text-mode)
(add-hook 'text-mode-hook 'turn-on-auto-fill)	;;auto fill
(setq visible-bell t)				;;visible bell

;;;;;;;;;;;;;;;;;;;;;; man interactive
(require 'man-commands)

;;;;;;;;;;;;;;;;;;;;;; load theme
(require 'color-theme)
(require 'fracc)
(color-theme-initialize)
(load-theme 'monokai t)

;;;;;;;;;;;;;;;;;;;;;; past / and / copy
(load "past-and-copy.el")
(provide 'config)

;;;;;;;;;;;;;;;;;;;;;; linum
(load "line-num.el")

;;;;;;;;;;;;;;;;;;;;;; ECB
(require 'ecb)
(setq ecb-layout-name "leftright2")
(setq ecb-show-sources-in-directories-buffer 'always)
(setq ecb-windows-width 40)

;;;;;;;;;;;;;;;;;;;;;; HTML/PHP-mode
(load "html-php.el")

;;;;;;;;;;;;;;;;;;;;;; gitgutter
(git-gutter-mode t)

;;;;;;;;;;;;;;;;;;;;;; backup
(defconst emacs-tmp-dir (format "%s/%s%s/" temporary-file-directory "emacs" (user-uid)))
(setq backup-directory-alist
      `((".*" . ,emacs-tmp-dir)))
(setq auto-save-file-name-transforms
      `((".*" ,emacs-tmp-dir t)))
(setq auto-save-list-file-prefix
      emacs-tmp-dir)

;;;;;;;;;;;;;;;;;;;;; tab-bar
(tabbar-mode t)
(load "tabbar-tweek.el")

;;;;;;;;;;;;;;;;;;;;; key binding
(load "key.el")


;;;;;;;;;;;;;;;;;;;;; header
(defun auto-make-header ()
  (and (zerop (buffer-size)) (not buffer-read-only) (buffer-file-name)
       (header-insert)))
(add-hook 'c-mode-common-hook   'auto-make-header)

;;;;;;;;;;;;;;;;;;;;; undo mode
(require 'undo-tree)
(global-undo-tree-mode)


;;;;;;;;;;;;;;;;;;;;; auto complete mode
(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "~/.emacs.d/ac-dict")
(ac-config-default)

;;;;;;;;;;;;;;;;;;;;; parentheses
(define-globalized-minor-mode global-highlight-parentheses-mode
  highlight-parentheses-mode
  (lambda ()
    (highlight-parentheses-mode t)))
(global-highlight-parentheses-mode t)
(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)

;;;;;;;;;;;;;;;;;;;;; delete trailing-whitespace
(add-hook 'before-save-hook 'delete-trailing-whitespace)
(add-hook 'before-save-hook 'del-end-whitespace)

;;;;;;;;;;;;;;;;;;;;; syntax checking
(add-hook 'after-init-hook #'global-flycheck-mode)
(setq flycheck-check-syntax-automatically '(mode-enabled save))

;;;;;;;;;;;;;;;;;;;; overflow char
(require 'whitespace)
(add-hook 'c-mode-hook (lambda() (setq whitespace-line-column 80))) ;; limit line length
;; (add-hook 'c-mode-hook 'whitespace-mode)

;;;;;;;;;; compile
(require 'compile)
(defun* get-closest-pathname (&optional (file "Makefile"))
  "Determine the pathname of the first instance of FILE starting from the current directory towards root.
This may not do the correct thing in presence of links. If it does not find FILE, then it shall return the name
of FILE in the current directory, suitable for creation"
  (let ((root (expand-file-name "/"))) ; the win32 builds should translate this correctly
    (expand-file-name file
		      (loop
		       for d = default-directory then (expand-file-name ".." d)
		       if (file-exists-p (expand-file-name file d))
		       return d
		       if (equal d root)
		       return nil))))
(add-hook 'c-mode-hook (lambda () (set (make-local-variable 'compile-command) (format "make -f %s" (get-closest-pathname)))))
(global-set-key (kbd "C-c <right>") 'tabbar-forward-tab)


;;;;;;;;;;;;;;;;;;;;; indent
(load "indent.el")


(provide 'config)
;;; config.el ends here
