;;; past-and-copy.el --- cpast and copy file
;;; commentary:
;******************************************************************************;
;                                                                              ;
;                                                         :::      ::::::::    ;
;    past-and-copy.el                                   :+:      :+:    :+:    ;
;                                                     +:+ +:+         +:+      ;
;    By: gfanton <gfanton@student.42.fr>            +#+  +:+       +#+         ;
;                                                 +#+#+#+#+#+   +#+            ;
;    Created: 2014/04/14 19:38:29 by gfanton           #+#    #+#              ;
;    Updated: 2014/04/14 19:51:11 by gfanton          ###   ########.fr        ;
;                                                                              ;
;******************************************************************************;
;;; code:
;;;;;;;;;;;;;;;;;;;;;; past and copy for osx
(defun copy-from-osx ()
"Copy from osx."
  (shell-command-to-string "pbpaste"))

(defun paste-to-osx (text &optional push)
"Past to osx."
  (let ((process-connection-type nil))
    (let ((proc (start-process "pbcopy" "*Messages*" "pbcopy")))
      (process-send-string proc text)
      (process-send-eof proc))))

(setq interprogram-cut-function 'paste-to-osx)
(setq interprogram-paste-function 'copy-from-osx)

(provide 'past-and-copy)
;;; past-and-copy.el ends here
