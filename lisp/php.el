;******************************************************************************;
;                                                                              ;
;                                                         :::      ::::::::    ;
;    php.el                                             :+:      :+:    :+:    ;
;                                                     +:+ +:+         +:+      ;
;    By: gfanton <marvin@42.fr>                     +#+  +:+       +#+         ;
;                                                 +#+#+#+#+#+   +#+            ;
;    Created: 2014/04/09 17:04:15 by gfanton           #+#    #+#              ;
;    Updated: 2014/04/14 19:50:25 by gfanton          ###   ########.fr        ;
;                                                                              ;
;******************************************************************************;

;; FIXME: This function still requires the setting of indent level.
;; It is currently hardcoded to four.

(defun php-insert-accesseur (type field)
  "Inserts a Php field, and getter/setter methods."
  (interactive "MType: \nMField: ")

  ;; If you don't like the automatic _ -prefix, then just set this
  ;; to whatever you want.  Empty is okay.
  (setq getsetprefix "_")
  (setq capfield (concat (capitalize (substring field 0 1)) (substring field 1)))
  (setq fieldb (concat getsetprefix field))
  (insert (concat "public function get" capfield "() { return ($this->" fieldb "); }\n"
                  "public function set" capfield "(" type " $"field " ) { $this->"fieldb " = $"field"; }\n"
                  ))
)

(defun php-insert-cdsg (classname)
  "Inserts a Php field"
  (interactive "MClass Name: ")
  (setq capfield (concat (capitalize (substring classname 0 1)) (substring classname 1)))
  (insert (concat "Class " capfield " {\n\n"
		  "\tpublic function doc(){echo file_get_contents('" capfield ".doc.txt'); }\n\n"
		  "\tpublic function __get ( $att )\n"
                  "\t{\n"
		  "\t\tprint('Attempt to access \\''. $att . '\\' atribute to \\'' . $value . '\\', this script should die' . PHP_EOL);\n"
		  "\t\treturn ;\n"
                  "\t}\n\n"
                  "\tpublic function __set ( $att, $value )\n"
                  "\t{\n"
		  "\t\tprint('Attempt to set \\''. $att . '\\' atribute to \\'' . $value . '\\', this script should die' . PHP_EOL);\n"
		  "\t\treturn ;\n"
                  "\t}\n"
		  "\tpublic function __construct ()\n"
                  "\t{\n"
	  	  "\t\treturn ;\n"
                  "\t}\n"
                  "\tpublic function __destruct ()\n"
                  "\t{\n"
		  "\t\treturn ;\n"
                  "\t}\n"
		  "\tpublic function __toString()\n"
		  "\t{\n"
       		  "\t\treturn sprintf('" capfield "');\n"
		  "\t}\n"
		  "}\n"
                  ))
)
(provide 'php.el)
;;; html-php.el ends here
