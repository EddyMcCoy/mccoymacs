;;; key.el --- key binding
;;; commentary:
;******************************************************************************;
;                                                                              ;
;                                                         :::      ::::::::    ;
;    key.el                                             :+:      :+:    :+:    ;
;                                                     +:+ +:+         +:+      ;
;    By: gfanton <gfanton@student.42.fr>            +#+  +:+       +#+         ;
;                                                 +#+#+#+#+#+   +#+            ;
;    Created: 2014/04/14 19:54:03 by gfanton           #+#    #+#              ;
;    Updated: 2014/05/27 12:18:37 by gfanton          ###   ########.fr        ;
;                                                                              ;
;******************************************************************************;
;;; code:

;;;;;;;;;;;;;;;;;;;;; keyboard

(global-set-key (kbd "DEL") 'backward-delete-char)
(setq-default c-backspace-function 'backward-delete-char)
(global-set-key [7]  'goto-line)
(global-unset-key (kbd "C-c C-c"))
(global-set-key (kbd "C-c C-c") 'comment-or-uncomment-region)
(global-set-key (kbd "C-c c") 'uncomment-region)
(global-set-key (kbd "ESC <up>") 'tabbar-forward-group) ; wheel up
(global-set-key (kbd "ESC <down>") 'tabbar-backward-group) ; wheel down
(global-set-key (kbd "ESC <left>") 'tabbar-backward-tab)
(global-set-key (kbd "ESC <right>") 'tabbar-forward-tab)
(global-set-key (kbd "C-c m") 'compile)
;; open file
(global-set-key (kbd "C-x C-o") 'ff-find-other-file)

;; activate term
(global-set-key (kbd "\e\et") 'multi-term)
;;term mode
(define-key global-map "\eO2D" (kbd "S-<left>"))
(define-key global-map "\eO2C" (kbd "S-<right>"))
(define-key global-map "\eO2A" (kbd "S-<up>"))
(define-key global-map "\eO2B" (kbd "S-<down>"))

(define-key global-map "\e[1;2D" (kbd "S-<left>"))
(define-key global-map "\e[1;2C" (kbd "S-<right>"))
(define-key global-map "\e[1;2A" (kbd "S-<up>"))
(define-key global-map "\e[1;2B" (kbd "S-<down>"))

;; active gitguter
(global-set-key (kbd "\e\eg") 'git-gutter:toggle)
;; activate and deactivate ecb
(global-set-key (kbd "\e\ee") 'ecb-activate)
(global-set-key (kbd "\e\eq") 'ecb-deactivate)
;; ;;; show/hide ecb window
(global-set-key (kbd "\e\ed") 'ecb-show-ecb-windows)
(global-set-key (kbd "\e\eh") 'ecb-hide-ecb-windows)
;; ;;; quick navigation between ecb windows
(global-set-key (kbd "\e\e`") 'ecb-goto-window-edit1)
(global-set-key (kbd "\e\ew") 'ecb-goto-window-edit1)
(global-set-key (kbd "\e\ef") 'ecb-goto-window-directories)
(global-set-key (kbd "\e\es") 'ecb-goto-window-sources)
(global-set-key (kbd "\e\eh") 'ecb-goto-window-history)
;; (global-set-key (kbd "C-#") 'ecb-goto-window-methods)
(global-set-key (kbd "\e\ez") 'ecb-goto-window-compilation)

;;;;;;;;;;;;;;;;;;;;; mouse

(require 'mouse)
(xterm-mouse-mode t)
(defun track-mouse (e))
(setq mouse-sel-mode t)
(setq xterm-mouse-mode t)
(setq mouse-wheel-mode t)
;; (setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time
;; (setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
;; (setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
(setq scroll-step 1) ;; keyboard scroll one line at a time
(global-set-key (kbd "<mouse-4>") 'scroll-down-line)
(global-set-key (kbd "<mouse-5>") 'scroll-up-line)
;; (global-set-key (kbd "<mouse-4>") 'ergoemacs-backward-block) ; wheel up
;; (global-set-key (kbd "<mouse-5>") 'ergoemacs-forward-block) ; wheel down


(provide 'key)
;;; key.el ends here
