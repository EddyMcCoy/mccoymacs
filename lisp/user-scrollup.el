;******************************************************************************;
;                                                                              ;
;                                                         :::      ::::::::    ;
;    user-scrollup.el                                   :+:      :+:    :+:    ;
;                                                     +:+ +:+         +:+      ;
;    By: Eddy <marvin@42.fr>                        +#+  +:+       +#+         ;
;                                                 +#+#+#+#+#+   +#+            ;
;    Created: 2014/03/20 02:00:30 by Eddy              #+#    #+#              ;
;    Updated: 2014/03/20 02:00:34 by Eddy             ###   ########.fr        ;
;                                                                              ;
;******************************************************************************;

(defun ergoemacs-forward-block ()
  "Move cursor forward to the beginning of next text block.
A text block is separated by 2 empty lines (or line with just whitespace).
In most major modes, this is similar to `forward-paragraph', but this command's behavior is the same regardless of syntax table."
  (interactive)
  (if (search-forward-regexp "\n[[:blank:]\n]*\n+" nil "NOERROR")
      (progn (backward-char))
    (progn (goto-char (point-max)) )
    )
  )

(defun ergoemacs-backward-block ()
  "Move cursor backward to previous text block.
See: `ergoemacs-forward-block'"
  (interactive)
  (if (search-backward-regexp "\n[\t\n ]*\n+" nil "NOERROR")
      (progn
	(skip-chars-backward "\n\t ")
	(forward-char 1)
	)
    (progn (goto-char (point-min)) )
    )
  )
