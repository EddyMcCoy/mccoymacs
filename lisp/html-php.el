;;; html-php.el --- html/php config
;;; commentary:
;******************************************************************************;
;                                                                              ;
;                                                         :::      ::::::::    ;
;    html-php.el                                        :+:      :+:    :+:    ;
;                                                     +:+ +:+         +:+      ;
;    By: gfanton <gfanton@student.42.fr>            +#+  +:+       +#+         ;
;                                                 +#+#+#+#+#+   +#+            ;
;    Created: 2014/04/14 19:43:08 by gfanton           #+#    #+#              ;
;    Updated: 2014/04/14 19:51:03 by gfanton          ###   ########.fr        ;
;                                                                              ;
;******************************************************************************;
;; FIXME: This function still requires the setting of indent level.
;; It is currently hardcoded to four.
;;; code:

(defun php-insert-accesseur (type field)
  "Inserts a Php field, and getter/setter methods."
  (interactive "MType: \nMField: ")

  ;; If you don't like the automatic _ -prefix, then just set this
  ;; to whatever you want.  Empty is okay.
  (setq getsetprefix "_")
  (setq capfield (concat (capitalize (substring field 0 1)) (substring field 1)))
  (setq fieldb (concat getsetprefix field))
  (insert (concat "public function get" capfield "() { return ($this->" fieldb "); }\n"
                  "public function set" capfield "(" type " $"field " ) { $this->"fieldb " = $"field"; }\n"
                  ))
)

(defun php-insert-cdsg (classname)
  "Inserts a Php field."
  (interactive "MClass Name: ")
  (setq capfield (concat (capitalize (substring classname 0 1)) (substring classname 1)))
  (insert (concat "Class " capfield " {\n\n"
		  "\tpublic function doc(){echo file_get_contents('" capfield ".doc.txt'); }\n\n"
		  "\tpublic function __get ( $att )\n"
                  "\t{\n"
		  "\t\tprint('Attempt to access \\''. $att . '\\' atribute to \\'' . $value . '\\', this script should die' . PHP_EOL);\n"
		  "\t\treturn ;\n"
                  "\t}\n\n"
                  "\tpublic function __set ( $att, $value )\n"
                  "\t{\n"
		  "\t\tprint('Attempt to set \\''. $att . '\\' atribute to \\'' . $value . '\\', this script should die' . PHP_EOL);\n"
		  "\t\treturn ;\n"
                  "\t}\n"
		  "\tpublic function __construct ()\n"
                  "\t{\n"
	  	  "\t\treturn ;\n"
                  "\t}\n"
                  "\tpublic function __destruct ()\n"
                  "\t{\n"
		  "\t\treturn ;\n"
                  "\t}\n"
		  "\tpublic function __toString()\n"
		  "\t{\n"
       		  "\t\treturn sprintf('" capfield "');\n"
		  "\t}\n"
		  "}\n"
                  ))
)

(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.jsp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))

(define-globalized-minor-mode my-global-rainbow-mode rainbow-mode
  (lambda () (rainbow-mode 1)))
(my-global-rainbow-mode 1)

(setq web-mode-engines-alist '(("php" . "\\.phtml\\'") ("blade" . "\\.blade\\.")) )
(setq web-mode-extra-auto-pairs '(("erb" . (("open" "close"))) ("php" . (("open" "close") ("open" "close"))) ))
(setq web-mode-enable-auto-pairing t)
(setq web-mode-enable-part-face t)
(setq web-mode-enable-css-colorization t)
(setq web-mode-enable-auto-pairing t)


(provide 'html-php)
;;; html-php.el ends here
