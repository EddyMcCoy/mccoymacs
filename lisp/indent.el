;;; indent.el --- indent level
;;; commentary:
;******************************************************************************;
;                                                                              ;
;                                                         :::      ::::::::    ;
;    indent.el                                          :+:      :+:    :+:    ;
;                                                     +:+ +:+         +:+      ;
;    By: gfanton <gfanton@student.42.fr>            +#+  +:+       +#+         ;
;                                                 +#+#+#+#+#+   +#+            ;
;    Created: 2014/04/14 19:20:57 by gfanton           #+#    #+#              ;
;    Updated: 2014/05/27 12:20:55 by gfanton          ###   ########.fr        ;
;                                                                              ;
;******************************************************************************;
;;; code:
;;;;;;;;;;;;;;;;;;;;;; load basic

(setq-default tab-width 4)
(setq-default indent-tabs-mode t)
(setq-default c-basic-offset 4)
(setq-default c-default-style "linux")
(c-set-offset 'substatement-open 0)   ;;; No indent for open bracket
;; (add-hook 'c-mode-common-hook 'google-set-c-style)
(setq-default tab-stop-list '(4 8 12 16 20 24 28 32 36 40 44 48 52 56 60
				64 68 72 76 80 84 88 92 96 100 104 108 112 116 120))

(provide 'indent)
;;; indent.el ends here
