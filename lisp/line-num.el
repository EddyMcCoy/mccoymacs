;;; line-num.el --- line numer mode
;;; commentary:
;******************************************************************************;
;                                                                              ;
;                                                         :::      ::::::::    ;
;    line-num.el                                        :+:      :+:    :+:    ;
;                                                     +:+ +:+         +:+      ;
;    By: Eddy <marvin@42.fr>                        +#+  +:+       +#+         ;
;                                                 +#+#+#+#+#+   +#+            ;
;    Created: 2014/04/14 22:46:32 by Eddy              #+#    #+#              ;
;    Updated: 2014/04/14 22:47:44 by Eddy             ###   ########.fr        ;
;                                                                              ;
;******************************************************************************;
;;; code:
(unless window-system
  (add-hook 'linum-before-numbering-hook
	    (lambda ()
	      (setq-local linum-format-fmt
			  (let ((w (length (number-to-string
					    (count-lines (point-min) (point-max))))))
			    (concat "%" (number-to-string w) "d"))))))

(defun linum-format-func (line)
  (concat
   (propertize (format linum-format-fmt line) 'face 'linum)
   (propertize " " 'face 'mode-line)))

(unless window-system
  (setq linum-format 'linum-format-func))
(global-linum-mode 1)
(setq line-number-mode t)
(setq column-number-mode t)
(display-time-mode 1)
