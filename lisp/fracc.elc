;ELC   
;;; Compiled by Eddy@EddyOSX.local on Mon Mar 31 13:40:01 2014
;;; from file /Users/guilhemfanton/fracc.el
;;; in Emacs version 24.3.1
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


#@54 Standard hook variable run when entering fracc-mode.
(defvar fracc-hook nil (#$ . 504))
#@56 Standard hook variable run when loading fracc package.
(defvar frac-mode-setup-hook nil (#$ . 598))
#@172 This buffer-specific variable is true whenever the fracc mode is on.
Its value designates the precise encoding to be used for French accents
(hence the name of the mode).
(defvar fracc-encoding nil (#$ . 705))
(make-variable-buffer-local 'fracc-encoding)
#@143 This variable defines the default encoding to be used by fracc when
no specific encoding is specified on the property-list of the major-mode.
(defvar fracc-default-encoding 'see-below (#$ . 967))
#@67 This is the keymap used by the fracc (French accents) minor mode.
(defvar fracc-keymap (make-sparse-keymap) (#$ . 1169))
(byte-code "\301\302\303#\210\301\304\305#\210\301\306\307#\210\301\310\311#\210\301\312\311#\210\301\313\314#\210\301\315\314#\210\301\316\317#\210\301\320\321#\210\301\322\314#\210\301\323\314#\210\301\324\317#\210\301\325\321#\210\301\326\327#\210\301\330\331#\207" [fracc-keymap define-key "'" fracc-acute "`" fracc-grave "^" fracc-circumflex "#" fracc-trema "\"" "a" fracc-ao-vowel "o" "u" fracc-u-vowel "i" fracc-i-vowel "A" "O" "U" "I" "<" fracc-opening-angle ">" fracc-closing-angle] 4)
(defalias 'fracc-acute #[nil "\300\301!\207" [fracc-handle-accent 39] 2 nil nil])
(defalias 'fracc-grave #[nil "\300\301!\207" [fracc-handle-accent 96] 2 nil nil])
(defalias 'fracc-circumflex #[nil "\300\301!\207" [fracc-handle-accent 94] 2 nil nil])
(defalias 'fracc-trema #[nil "\300\301!\207" [fracc-handle-accent 35] 2 nil nil])
(defalias 'fracc-ao-vowel #[nil "\300\301!\207" [fracc-handle-accent 97] 2 nil nil])
(defalias 'fracc-u-vowel #[nil "\300\301!\207" [fracc-handle-accent 117] 2 nil nil])
(defalias 'fracc-i-vowel #[nil "\300\301!\207" [fracc-handle-accent 105] 2 nil nil])
(defalias 'fracc-opening-angle #[nil "\300\301!\207" [fracc-handle-accent 60] 2 nil nil])
(defalias 'fracc-closing-angle #[nil "\300\301!\207" [fracc-handle-accent 62] 2 nil nil])
#@310 Handle French accent and convert them to some appropriate encoding depending
on the major mode. An argument can be given to force the encoding to use.
Possible encodings are:
	fracc-no-encoding
	fracc-ISO-8859-encoding
	fracc-tex-encoding
	fracc-bibtex-encoding
	fracc-html-encoding
	fracc-Mac-encoding
	...

(defalias 'fracc-mode #[(encoding) "\305\300!\210\303\236\204 \303	BB\n\306\236\204 \306\fBB\307\310!\210\311\207" [minor-mode-map-alist fracc-keymap encoding fracc-encoding fracc-isearch-keymap make-variable-buffer-local fracc-isearch-buffer run-hooks fracc-mode-hook t] 2 (#$ . 2575) (list (fracc-select-default-encoding))])
#@391 Select the encoding of French accents based on the current major mode.
This encoding is stored in the property list of the major-mode under
key: fracc-encoding. When you load the fracc package, default fracc modes
are defined for (plain-)(la)(sli)(bib)tex-mode and html-(helper-)mode. If no
specific encoding is given then use the encoding specified by the
fracc-default-encoding variable.
(defalias 'fracc-select-default-encoding #[nil "\303N\211\206	 \n)\207" [major-mode encoding fracc-default-encoding fracc-encoding] 3 (#$ . 3227)])
#@1073 This function takes an accent, extracts from the current encoding the
possible contexts where an action is possible, checks if one of this
context is met then remove that context and perform the action.
  The encoding is formatted as an alist:
     (case-fold-search-value (accent-char . clauses) ... )
Each clause is ( context-string  . actions )
  means if preceding characters are matched by context-string then remove
  them and process the actions. NOTE: context-string is not a regexp.
Actions may be ( . "string" )
  which means insert string.
            or ()
  which means insert the accent
            or ( "string" . actions)
  which means insert string then continue to process actions.
            or ( expression . actions)
  which means evaluate expression then continue to process actions.
  An interesting particular case is:
            or ( "\&" . actions)
  which means insert the context-string and continue to process actions.
  This works since all strings are inserted with replace-match,
  therefore they may use \& to match the context-string.

(defalias 'fracc-handle-accent #[(accent) "	A\236\211:\203U \nA	@\211?\306\211\f\306`\306:\203D b\210@@\211G\f@AA\306\307\310\217\210\202\" \203M \311\202P \312\313!.\202X \312\313!)\207" [accent fracc-encoding specs clauses case-fold-search fixedcase nil (byte-code "[u\210\306	\n\307#\203Y \310 	\311\307:\203? @;\2030 \312@\f$\210\2026 \313@!\210A\211\202 ;\203Q \312\f$\210\202U \314\315!\210*\202\\ \nb\210\311\207" [size *context-string* dot cs md clauses search-forward t match-data nil fracc-replace-match eval self-insert-command 1 performedp action fixedcase] 6) ((error (goto-char dot))) nothing self-insert-command 1 size *context-string* action dot performedp] 4 (#$ . 3775)])
#@151 This function acts as replace-match to replace the *context-string* with
REGEXP in the buffer. It may be used more than once due to multiple actions.
(defalias 'fracc-replace-match #[(regexp md cs fixedcase) "@A@	\211\232\203 \nc\210`\306\223\210\307!\210\310	\"\210\f`\306\223+\207" [md cs *context-string* *end* *beg* regexp nil store-match-data replace-match fixedcase] 4 (#$ . 5602)])
#@62 Boolean telling if fracc is active while isearching strings.
(defvar fracc-isearch-wanted nil (#$ . 6008))
#@64 This is the keymap used by isearch with French accent support.
(defvar fracc-isearch-keymap nil (#$ . 6121))
#@62 Build the keymap used by isearch with French accent support.
(defalias 'fracc-build-isearch-keymap #[nil "\206` \303	!\304\n\305\306#\210\304\n\305\306#\210\304\n\307\306#\210\304\n\310\306#\210\304\n\311\306#\210\304\n\312\306#\210\304\n\313\306#\210\304\n\314\306#\210\304\n\315\306#\210\304\n\316\306#\210\304\n\317\306#\210\304\n\320\306#\210\304\n\321\306#\210\304\n\322\306#\210\n\211)\207" [fracc-isearch-keymap isearch-mode-map map copy-keymap define-key "'" fracc-isearch-handle-char "`" "^" "#" "\"" "a" "o" "u" "i" "A" "O" "U" "I"] 4 (#$ . 6236)])
(byte-code "\300\301!\210\302\301\303\"\207" [make-variable-buffer-local fracc-isearch-buffer set-default nil] 3)
#@53 This function handles French accents under isearch.
(defalias 'fracc-isearch-handle-char #[nil "\304 \212	q\210\305 \210\nc\210\306!\210\307 )\211\310 )\207" [accent fracc-isearch-buffer isearch-string isearch-message isearch-last-command-char erase-buffer fracc-handle-accent buffer-string isearch-search-and-update] 3 (#$ . 6918) nil])
#@115 This function installs the necessary support for French accents with the
incremental search package (isearch.el).
(defalias 'fracc-isearch-hook #[nil "\304 	\305\306!\212q\210\307 \210\310\n!\210*\311\207" [overriding-local-map fracc-encoding current-fracc-encoding fracc-isearch-buffer fracc-build-isearch-keymap get-buffer-create " *Fracc-Isearch* " erase-buffer fracc-mode t] 2 (#$ . 7267)])
#@114 This function removes the necessary support for French accents with the
incremental search package (isearch.el).
(defalias 'fracc-isearch-end-hook #[nil "\301\211\207" [fracc-isearch-buffer nil] 2 (#$ . 7673)])
(byte-code "\203 \301\302\303\"\210\301\304\305\"\210\301\207" [fracc-isearch-wanted add-hook isearch-mode-hook fracc-isearch-hook isearch-mode-end-hook fracc-isearch-end-hook] 3)
#@50 This is the null encoding where nothing is done.
(defvar fracc-no-encoding '(nil) (#$ . 8073))
#@126 This is the French encoding towards ISO 8859 accented letters.
This list was adapted from Cedric Beust's 8bits-mode package.
(defvar fracc-ISO-8859-encoding '(nil (39 ("E" . "\311") ("e" . "\351")) (96 ("A" . "\300") ("E" . "\310") ("U" . "\331") ("a" . "\340") ("e" . "\350") ("u" . "\371")) (94 ("A" . "\302") ("E" . "\312") ("I" . "\316") ("O" . "\324") ("U" . "\333") ("a" . "\342") ("e" . "\352") ("i" . "\356") ("o" . "\364") ("u" . "\373")) (35 ("E" . "\313") ("I" . "\317") ("U" . "\334") ("e" . "\353") ("i" . "\357") ("u" . "\374")) (97 ("c," "\347") ("C," "\307")) (117 ("c," "\347") ("C," "\307")) (60 ("<" . "\253~")) (62 (">" . "~\273"))) (#$ . 8175))
#@59 This is the French encoding towards Mac accented letters.
(defvar fracc-Mac-encoding '(nil (39 ("E" . "\203") ("e" . "\216") ("'" . "\323")) (96 ("A" . "\313") ("a" . "\210") ("e" . "\217") ("u" . "\235") ("`" . "\322")) (94 ("a" . "\211") ("e" . "\220") ("i" . "\224") ("o" . "\231") ("u" . "\236")) (35 ("I" . "\225") ("U" . "\206") ("e" . "\221") ("i" . "\225") ("u" . "\237")) (97 ("c," "\215") ("C," "\202")) (117 ("c," "\215") ("C," "\202") ("OE" "\316") ("oe" "\317")) (105 ("OE" "\316") ("oe" "\317"))) (#$ . 8847))
#@58 This is the encoding used for French accent in TeX mode.
(defvar fracc-tex-encoding '(t (39 ("e''" . "\\\\'\\&") ("\\'e" "e'") ("e" . "\\\\'\\&")) (96 ("\\`a" "\\&") ("\\`e" "\\&") ("\\`o" "\\&") ("\\`u" "\\&") ("a" . "\\\\`\\&") ("e" . "\\\\`\\&") ("u" . "\\\\`\\&")) (94 ("\\^a" "\\&") ("\\^e" "\\&") ("\\^i" "\\&") ("\\^o" "\\&") ("\\^u" "\\&") ("a" . "\\\\^\\&") ("e" . "\\\\^\\&") ("i" . "\\\\^\\\\\\&{}") ("o" . "\\\\^\\&") ("u" . "\\\\^\\&")) (35 ("\\\"e" "\\&") ("\\\"i" "\\&") ("\\\"u" "\\&") ("e" . "\\\\\"\\&") ("i" . "\\\\\"\\\\\\&{}") ("u" . "\\\\\"\\&")) (97 ("c," "\\\\c{c}")) (117 ("c," "\\\\c{c}") ("oe" "\\\\\\&{}")) (105 ("oe" "\\\\\\&{}")) (33 ("~" "~") (" " "~") ("" "~"))) (#$ . 9377))
#@157 This is the encoding for an 8 bits display in TeX mode.
This list was adapted from Cedric Beust's 8bits-mode package
by Geoffroy VILLE <ville@cena.dgac.fr>
(defvar fracc-8bits-tex-encoding '(nil (39 ("E" . "\311") ("e" . "\351") ("'" . "\264")) (96 ("A" . "\300") ("E" . "\310") ("U" . "\331") ("a" . "\340") ("e" . "\350") ("u" . "\371")) (94 ("A" . "\302") ("E" . "\312") ("I" . "\316") ("O" . "\324") ("U" . "\333") ("a" . "\342") ("e" . "\352") ("i" . "\356") ("o" . "\364") ("u" . "\373")) (35 ("E" . "\313") ("I" . "\317") ("U" . "\334") ("e" . "\353") ("i" . "\357") ("u" . "\374")) (97 ("c," "\347") ("C," "\307")) (117 ("c," "\347") ("C," "\307") ("oe" "\\\\oe{}") ("OE" "\\\\OE{}")) (105 ("oe" "\\\\oe{}") ("OE" "\\\\OE{}"))) (#$ . 10092))
#@143 This is the encoding used for French accent in BibTeX mode.
The difference with TeX mode is that accented letters are wrapped inside
{ and }.
(defvar fracc-bibtex-encoding '(t (39 ("e''" . "{\\\\'e}''") ("{\\'e}" "e'") ("e" . "{\\\\'e}")) (96 ("a" . "{\\\\`\\&}") ("e" . "{\\\\`\\&}") ("u" . "{\\\\`\\&}")) (94 ("a" . "{\\\\^\\&}") ("e" . "{\\\\^\\&}") ("i" . "{\\\\^\\\\\\&}") ("o" . "{\\\\^\\&}") ("u" . "{\\\\^\\&}")) (35 ("e" . "{\\\\\"\\&}") ("i" . "{\\\\\"\\\\\\&}") ("u" . "{\\\\\"\\&}")) (97 ("c," "{\\\\c{c}}")) (117 ("c," "{\\\\c{c}}") ("oe" "{\\\\\\&}")) (105 ("oe" "{\\\\\\&}"))) (#$ . 10849))
#@63 This is the encoding used for French accent in HTML document.
(defvar fracc-html-encoding '(nil (39 ("e''" . "&eacute;''") ("&eacute;" "e'") ("e" . "&eacute;") ("E''" . "&Eacute;''") ("&Eacute;" "E'") ("E" . "&Eacute;")) (96 ("a" . "&agrave;") ("e" . "&egrave;") ("u" . "&ugrave;") ("A" . "&Agrave;") ("E" . "&Egrave;") ("U" . "&Ugrave;")) (94 ("a" . "&acirc;") ("e" . "&ecirc;") ("i" . "&icirc;") ("o" . "&ocirc;") ("u" . "&ucirc;") ("A" . "&Acirc;") ("E" . "&Ecirc;") ("I" . "&Icirc;") ("O" . "&Ocirc;") ("U" . "&Ucirc;")) (35 ("e" . "&euml;") ("i" . "&iuml;") ("u" . "&uuml;") ("E" . "&Euml;") ("I" . "&Iuml;") ("U" . "&Uuml;")) (97 ("c," "&ccedil;") ("C," "&Ccedil;")) (117 ("c," "&ccedil;") ("C," "&Ccedil;") ("oe" "&#156;") ("OE" "&#140;")) (105 ("oe" "&#156;") ("OE" "&#140;"))) (#$ . 11461))
(byte-code "\305\306\307#\210\305\310\307#\210\305\311\307#\210\305\312\307#\210\305\313\307	#\210\305\314\307\n#\210\305\315\307\n#\210\316\317!\210\320\321!\207" [fracc-tex-encoding fracc-bibtex-encoding fracc-html-encoding fracc-ISO-8859-encoding fracc-default-encoding put tex-mode fracc-encoding plain-tex-mode latex-mode slitex-mode bibtex-mode html-mode html-helper-mode run-hooks fracc-mode-setup-hook provide fracc] 4)
