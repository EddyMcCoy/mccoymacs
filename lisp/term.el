;;; term.el --- term mode
;;; commentary:
;******************************************************************************;
;                                                                              ;
;                                                         :::      ::::::::    ;
;    term.el                                            :+:      :+:    :+:    ;
;                                                     +:+ +:+         +:+      ;
;    By: gfanton <gfanton@student.42.fr>            +#+  +:+       +#+         ;
;                                                 +#+#+#+#+#+   +#+            ;
;    Created: 2014/04/14 19:49:04 by gfanton           #+#    #+#              ;
;    Updated: 2014/04/14 19:55:25 by gfanton          ###   ########.fr        ;
;                                                                              ;
;******************************************************************************;
;;; code:
(require 'ansi-color)
(setq multi-term-program "/bin/zsh")
(defun colorize-compilation-buffer ()
  (read-only-mode)
  (ansi-color-apply-on-region (point-min) (point-max))
  (read-only-mode))
(add-hook 'compilation-filter-hook 'colorize-compilation-buffer)
(add-hook 'term-mode-hook
	  (lambda ()
	    (setq term-buffer-maximum-size 10000)))
(add-hook 'term-mode-hook
	  (lambda ()
	    (setq show-trailing-whitespace nil)
	    (autopair-mode -1)))
(add-hook 'term-mode-hook
	  (lambda ()
	    (add-to-list 'term-bind-key-alist '("M-[" . multi-term-prev))
	    (add-to-list 'term-bind-key-alist '("M-]" . multi-term-next))
	    (define-key term-raw-map (kbd "C-y") 'term-paste)
	    (define-key term-raw-map (kbd "<mouse-1>") 'select-window)
	    (define-key term-raw-map (kbd "<mouse-2>") 'nil)
	    (define-key term-raw-map (kbd "<mouse-4>") 'scroll-down-line)
	    (define-key term-raw-map (kbd "<mouse-5>") 'scroll-up-line)
	    ))

(setq system-uses-terminfo nil) ;; Use Emacs terminfo, not system terminfo
(add-hook 'term-mode-hook '(lambda () (setq truncate-lines t)))

(provide 'term)
;;; term.el ends here
